<?php

namespace Geacl\AuditPass;

use Illuminate\Database\Eloquent\Model;

class OldPassword extends Model
{
    protected $table = 'old_passwords';

    protected $fillable = ['user_id', 'password'];

    protected $hidden = ['password'];
}
