<?php


namespace Geacl\AuditPass;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class GeaclPasswordChecker
{
    public $isValid;

    public function __construct()
    {
        $this->isValid = true;
    }

    /**
     * Recibe un usuario  que extienda un modelo clasico, la nueva password y el primary key en caso de que sea modificado
     * y reterona un true en caso de que la password pueda ser usada, en caso contrario retornara un false
     *
     * @param $user
     * @param string $password
     * @param string $primaryKey
     * @param string $passwordField
     *
     * @return GeaclPasswordChecker
     */
    public function verify($user, string $password = "", string $primaryKey = "id", string $passwordField = "password")
    {

        if (!$this->validPassword($password)) {
            $this->isValid = false;
        }

        $passwords = OldPassword::where('user_id', $user->$primaryKey)->get();

        foreach ($passwords as $pass) {
            $var = Hash::check($password, $pass->password);
            if ($var) {
                $this->isValid = false;
            }
        }

        if ($this->isValid) {

        }

        return $this;

    }

    protected function validPassword($password)
    {
        $arr = ["password" => $password];

        $validator = Validator::make($arr, [
            'password' => ["required", "min:8"]
        ]);

        if ($validator->fails()) {
            return false;
        }
        return true;
    }


    public function registerOldPassword(int $id, string $password)
    {

        OldPassword::create([
            'user_id' => $id,
            'password' => $password
        ]);

    }
}
