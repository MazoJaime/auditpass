<?php

namespace Geacl\AuditPass\Listeners;

use Geacl\AuditPass\Events\RegisterUserChangeEvent;
use Geacl\AuditPass\PasswordChange;

class RegisterChangeListener
{

    public function handle(RegisterUserChangeEvent $event)
    {

        $mensaje =  "{$event->evento} por usuario id: {$event->usuario_accion}  al usuario id: {$event->usuario_afectado}";

        PasswordChange::create([
            'event' => $event->evento,
            'user_id' => $event->usuario_accion->id,
            'description' => $mensaje
        ]);
    }

}
