<?php

Route::get('session/refresh', '\Geacl\AuditPass\Http\Controllers\SessionFreshenerController@refresh')
->middleware(['web','auth']);
