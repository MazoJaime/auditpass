$("#txtnew_password1").keypress(function (e) {
    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{8,}).*", "g");
    if (false == enoughRegex.test($(this).val())) {
        $('#txtnew_password1').prop("style", "border:1px solid red");
        $('#alertpass').prop("style", "color:red");
    } else if (strongRegex.test($(this).val())) {
        $('#txtnew_password1').prop("style", "border:1px solid green");
        $('#alertpass').prop("style", "color:green");
    } else if (mediumRegex.test($(this).val())) {
        $('#txtnew_password1').prop("style", "border:1px solid yellow");
        $('#alertpass').prop("style", "color:yellow");
    }
    return true;
});


/*
    Esta función valida el formato del password ingresado
    recibe como parámetros el password del usuario, la confirmación del password
    la longitud del password y la fortaleza que debe tener:
    d = débil 8 caracteres mínimo
    m = media 8 caracteres mínimo combinando números y letras
    f = fuerte 8 caracteres mínimo, almenos una mayúscula, una minúscula, un número y un caracter especial
*/
function valida_password(password1, password2, longpassreq = 8, fortpass = "f") {

    var longpass = password1.length;

    var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
    var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
    var enoughRegex = new RegExp("(?=.{8,}).*", "g");

    // Validamos la longitud mínima del password según el tipo de usuario admin = 12, otros = 8
    if (longpass < longpassreq) {
        mostrarMensaje("El password no cumple con la longitud mínima requerida");
        return false;
    }
    // El password y su confirmación deben ser iguales
    if (password1 != password2) {
        mostrarMensaje("Los passwords no concuerdan", "danger");
        return false;
    }
    // El password no puede ir en blanco
    if (password1 == "") {
        mostrarMensaje("Debe ingresar el password del usuario", "danger");
        return false;
    }
    // Se verifica que el password cumpla con los requisitos mínimos de formato
    switch (fortpass) {
        case "d": // si cumple con cualquiera de los niveles pasa
            if (enoughRegex.test(password1) || mediumRegex.test(password1) || strongRegex.test(password1)) {
                return true;
            }
            break;
        case "m": // si cumple con seguridad media o fuerte pasa
            if (mediumRegex.test(password1) || strongRegex.test(password1)) {
                return true;
            }
            break;
        case "f": // solo si cumple con seguridad fuerte pasa
            if (strongRegex.test(password1)) {
                return true;
            }
            break;
    }
    mostrarMensaje("El password no cumple con los requisitos mínimos de seguridad", "danger");
    return false;
}
