$(document).ready(function () {
    setInterval(function () {
        var close = false;
        Swal.fire({
            html: 'La sesion se cerrara de forma automatica en <b></b> segundos.',
            showDenyButton: true,
            showCancelButton: false,
            confirmButtonClass: 'swal-ok',
            denyButtonClass: 'swal-ok',
            customClass: 'swal-content-ok',
            width: 800,
            confirmButtonText: `Mantener sesion`,
            denyButtonText: `Cerrar ahora`,
            timer: 1000 * 30,
            timerProgressBar: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            didOpen: () => {
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Math.round(Swal.getTimerLeft() / 1000);
                        }
                    }
                }, 1000)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        })
            .then(function (res) {

                if (res.isConfirmed) {
                    fetch('/session/refresh').then(function (response) {
                        console.log(response);
                        return response.json()
                    }).then(console.log);

                }
                if (res.isDenied || res.dismiss == "backdrop") {
                    close = true;
                }
                if (res.dismiss === Swal.DismissReason.timer) {
                    setInterval(function () {
                    }, 2000);
                    window.location.reload();
                }
                if (close) {
                    document.getElementById('logout-form').submit();
                }

            });

    }, 1000 * 150);
})
