<?php


namespace Geacl\AuditPass\Events;


use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RegisterUserChangeEvent
{
    use Dispatchable, SerializesModels;

    public $evento;
    public $usuario_accion;
    public $usuario_afectado;

    /**
     * RegisterUserChangeEvent constructor.
     * @param string $evento
     * @param $usuario_accion
     * @param $usuario_afectado
     */
    public function __construct(string $evento, $usuario_accion, $usuario_afectado)
    {
        $this->evento = $evento;
        $this->usuario_accion = $usuario_accion;
        $this->usuario_afectado = $usuario_afectado;

    }

}
