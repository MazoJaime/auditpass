<?php

namespace Geacl\AuditPass;

use Illuminate\Database\Eloquent\Model;

class PasswordChange extends Model
{
    protected $table = 'password_changes';

    protected $fillable = ['event', 'user_id', 'description'];



}
