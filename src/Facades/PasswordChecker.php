<?php

namespace Geacl\AuditPass\Facades;

use Illuminate\Support\Facades\Facade;

class PasswordChecker extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'geacl';
    }

}
