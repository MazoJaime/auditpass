<?php

namespace Geacl\AuditPass\Http\Controllers;

use Illuminate\Http\Request;

class SessionFreshenerController extends Controller
{

    public function refresh(Request $request){

        $user  = auth()->user();
        $request->session()->regenerate();

        return ['success' => 'regenerada', 'user' => $user];
    }


}
