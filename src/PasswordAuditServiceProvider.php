<?php

namespace Geacl\AuditPass;

use Geacl\AuditPass\Events\RegisterUserChangeEvent;
use Geacl\AuditPass\Listeners\RegisterChangeListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Geacl\AuditPass\Facades\PasswordChecker;

class PasswordAuditServiceProvider extends ServiceProvider
{
    protected $listen = [
        RegisterUserChangeEvent::class => [
            RegisterChangeListener::class,
        ]
    ];


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->loadRoutesFrom(__DIR__.'/routes/routes.php');


        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        if ($this->app->runningInConsole()) {
            if (!class_exists('CreatePasswordChangesTable') && !class_exists('CreateOldPasswordsTable')) {
                $this->publishes([
                    __DIR__ . '/migrations/create_password_changes_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_password_changes_table.php'),
                    __DIR__ . '/migrations/create_old_passwords_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_old_passwords_table.php'),
                ], 'migrations-GEACL');
            }

            $this->publishes([
                __DIR__.'/public' => public_path('vendor/geacl'),
            ], 'geaclJs');
        }

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->alias(GeaclPasswordChecker::class, 'geacl');
    }
}
